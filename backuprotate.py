#!/usr/bin/python

import os, sys, getopt

class BackupRotate:

	def __init__(self, path="/mnt/backup/databases", number=120, dryRun=False):
		self.path = path
		self.number = number
		self.dryRun = dryRun

	def set_path(self, path):
		self.path = path

	def set_number(self, number):
		self.number = number

	def set_dry_run(self, dryRun):
		self.dryRun = dryRun

	def del_old_backups(self):
		toDelete = sorted(os.listdir(self.path))
		numberOfDeletedFiles = len(toDelete) - self.number
		if (numberOfDeletedFiles > 0):
			for file in toDelete[0: numberOfDeletedFiles]:
				if (self.dryRun):
					print "I would delete:", file
				else:
					print "Delete", self.path + '/' + file
					os.remove(self.path + '/' + file)
                else:
                        print "Nothing to do!"

def print_help():
	print 'backuprotate.py'
	print 'optional parameters: '
	print '-h --help show help'
	print '-d --dryRun don\'t delete any files'
	print '-p= --path= specifiy the path of the backup files'
	print '-n= --number= specifiy the number of kept backups'

def main(argv):

	backupRotate = BackupRotate()

	try:
		opts, args = getopt.getopt(argv,"hdp:n:",["help","dryRun", "path=", "number="])
	except getopt.GetoptError:
		print_help()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h', '--help'):
			print_help()
			sys.exit()
		elif opt in ("-p", "--path"):
			backupRotate.set_path(arg)
		elif opt in ("-d", "--dryRun"):
			backupRotate.set_dry_run(True)
		elif opt in ("-n", "--number"):
			backupRotate.set_number(int(arg))

	backupRotate.del_old_backups()

if __name__ == "__main__":
	main(sys.argv[1:])
